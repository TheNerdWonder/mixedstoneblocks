﻿using System.Collections.Generic;

namespace MixedStoneBlocks
{
    using HarmonyLib;
    using UnityEngine;
    using Verse;

    [StaticConstructorOnStartup]
    public class MixedStoneBlocksPatcher
    {
        static MixedStoneBlocksPatcher()
        {
            var harmony = new Harmony("nerdwonder.mixedStoneBlocks");
            harmony.PatchAll();

            Debug.Log("Mixed Stone Blocks loaded and patched.");
        }
    }
}