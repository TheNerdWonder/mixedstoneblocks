﻿using RimWorld;

namespace MixedStoneBlocks
{
    using System.Reflection;
    using HarmonyLib;
    using UnityEngine;
    using Verse;

    [StaticConstructorOnStartup]
    public static class WidgetsPatch
    {
        private static readonly Texture2D MixedWallIcon;
        private static readonly Texture2D MixedDoorIcon;
        private static readonly Texture2D MixedAutodoorIcon;

        static WidgetsPatch()
        {
            MixedWallIcon = PatchGraphics.MixedWallIcon;
            MixedDoorIcon = PatchGraphics.MixedDoorIcon;
            MixedAutodoorIcon = PatchGraphics.MixedAutodoorIcon;
        }

        [HarmonyPatch(
            typeof(Widgets),
            nameof(Widgets.ThingIcon), 
            typeof(Rect), typeof(ThingDef), typeof(ThingDef), typeof(ThingStyleDef), typeof(float), typeof(Color?), typeof(int?))]
        public static class ThingIconPatch
        {
            [HarmonyPrefix]
            static bool Prefix(Rect rect, ThingDef thingDef, ThingDef stuffDef, ThingStyleDef thingStyleDef, float scale, Color? color, int? graphicIndexOverride)
            {
                if (stuffDef != MixedStoneBlockDefs.MixedStoneBlocks)
                    return true;
                
                bool overrideIcon = false;
                Texture newTexture = null;
                
                // Determine if we need to override the normal icon logic
                // Probably only Things with static custom graphics
                if (thingDef == ThingDefOf.Wall)
                {
                    overrideIcon = true;
                    newTexture = MixedWallIcon;
                }
                else if (thingDef == ThingDefOf.Door)
                {
                    overrideIcon = true;
                    newTexture = MixedDoorIcon;
                }
                else 
                if (thingDef == MixedStoneBlockDefs.Autodoor)
                {
                    overrideIcon = thingStyleDef.Graphic == null;
                    newTexture = MixedAutodoorIcon;
                }
                
                if (overrideIcon)
                {
                    GUI.color = Color.white;
                    var parms = new object[] {rect, thingDef, newTexture, thingDef.uiIconAngle, scale};
                    typeof(Widgets).GetMethod("ThingIconWorker", BindingFlags.Static | BindingFlags.NonPublic)
                                  ?.Invoke(null, parms);
                }
                
                return !overrideIcon; 
            }
        }
    }
}