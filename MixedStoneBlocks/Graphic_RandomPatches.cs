﻿using System;
using System.Collections.Generic;

namespace MixedStoneBlocks
{
    using HarmonyLib;
    using UnityEngine;
    using Verse;

    public static class Graphic_RandomPatches
    {
        [HarmonyPatch(
            typeof(Graphic_Random),
            nameof(Graphic_Random.GetColoredVersion))]
        public static class Graphic_Random_GetColoredVersionPatch
        {
            [HarmonyPrefix]
            static bool Prefix(Shader newShader, Color newColor, Color newColorTwo, Graphic __instance, ref Graphic __result)
            {
                // Remove stupid check for colorTwo == White
                __result = GraphicDatabase.Get<Graphic_Random>(__instance.path, newShader, __instance.drawSize, newColor, newColorTwo, __instance.data);

                return false;
            }
        }
    }
}