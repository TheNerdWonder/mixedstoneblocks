﻿using System;
using System.Collections.Generic;

namespace MixedStoneBlocks
{
    using HarmonyLib;
    using RimWorld;
    using System.Linq;
    using UnityEngine;
    using Verse;

    public static class Graphic_AppearancesPatches
    {
        [HarmonyPatch(
            typeof(Graphic_Appearances),
            nameof(Graphic_Appearances.Init))]
        public static class Graphic_Appearances_InitPatch
        {
            // Add support for non-white colorTwo to Graphic_Appearances

            [HarmonyPrefix]
            static bool Prefix(GraphicRequest req, Graphic_Appearances __instance, ref Graphic[] ___subGraphics)
            {
                __instance.data = req.graphicData;
                __instance.path = req.path;
                __instance.color = req.color;
                __instance.colorTwo = req.colorTwo; // <---
                __instance.drawSize = req.drawSize;
                var defsListForReading = DefDatabase<StuffAppearanceDef>.AllDefsListForReading;
                ___subGraphics = new Graphic[defsListForReading.Count];
                for (var index = 0; index < ___subGraphics.Length; ++index)
                {
                    var stuffAppearance = defsListForReading[index];
                    var folderPath = req.path;
                    if (!stuffAppearance.pathPrefix.NullOrEmpty())
                        folderPath = stuffAppearance.pathPrefix + "/" + folderPath.Split('/').Last();
                    var texture2D = ContentFinder<Texture2D>.GetAllInFolder(folderPath).FirstOrDefault(x => x.name.EndsWith(stuffAppearance.defName));
                    if (texture2D != null)
                        ___subGraphics[index] = GraphicDatabase.Get<Graphic_Single>(folderPath + "/" + texture2D.name, req.shader, __instance.drawSize, __instance.color, __instance.colorTwo); // <---
                }
                for (int index = 0; index < ___subGraphics.Length; ++index)
                {
                    if (___subGraphics[index] == null)
                        ___subGraphics[index] = ___subGraphics[StuffAppearanceDefOf.Smooth.index];
                }

                return false;
            }
        }

        [HarmonyPatch(
            typeof(Graphic_Appearances),
            nameof(Graphic_Appearances.GetColoredVersion))]
        public static class Graphic_Appearances_GetColoredVersionPatch
        {
            [HarmonyPrefix]
            static bool Prefix(Shader newShader, Color newColor, Color newColorTwo, Graphic __instance, ref Graphic __result)
            {
                // Remove stupid check for colorTwo == White
                __result = GraphicDatabase.Get<Graphic_Appearances>(__instance.path, newShader, __instance.drawSize, newColor, newColorTwo, __instance.data);

                return false;
            }
        }
    }
}