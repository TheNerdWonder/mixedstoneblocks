﻿using UnityEngine;

namespace MixedStoneBlocks
{
    using HarmonyLib;
    using RimWorld;
    using Verse;

    public static class BuildableDefPatch
    {
        [HarmonyPatch(
            typeof(BuildableDef),
            "PostLoad")]
        public static class PostLoadPatch
        {
            [HarmonyPostfix]
            static void Postfix(BuildableDef __instance)
            {
                var thingDef = __instance as ThingDef;
                if (thingDef == null || !thingDef.MadeFromStuff)
                    return;

                var stuff = GenStuff.DefaultStuffFor(__instance);

                if (stuff != MixedStoneBlockDefs.MixedStoneBlocks)
                    return;

                StoneDefs.LoadDefs();
                StoneDefs.GetColors(0, out var color, out _, false);
                thingDef.uiIconColor = color;
            }
        }

        [HarmonyPatch(
            typeof(BuildableDef),
            "ResolveIcon")]
        public static class ResolveIconPatch
        {
            [HarmonyPostfix]
            static void Postfix(BuildableDef __instance)
            {
                var thingDef = __instance as ThingDef;
                if (thingDef == null || !thingDef.MadeFromStuff)
                    return;
                
                var stuff = GenStuff.DefaultStuffFor(__instance);

                if (stuff != MixedStoneBlockDefs.MixedStoneBlocks)
                    return;
                
                StoneDefs.LoadDefs();
                StoneDefs.GetColors(0, out var color, out _, false);
                thingDef.uiIconColor = color;
            }
        }

        [HarmonyPatch(
            typeof(BuildableDef),
            nameof(BuildableDef.GetColorForStuff))]
        public static class GetColorForStuffPatch
        {
            [HarmonyPrefix]
            static bool Postfix(ThingDef stuff, ref Color __result)
            {
                if (stuff != MixedStoneBlockDefs.MixedStoneBlocks)
                    return true;

                StoneDefs.LoadDefs();
                StoneDefs.GetColors(0, out var color, out _, false);
                __result = color;

                return false;
            }
        }
    }
}