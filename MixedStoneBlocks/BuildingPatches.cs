﻿using System;
using System.Collections.Generic;

namespace MixedStoneBlocks
{
    using HarmonyLib;
    using UnityEngine;
    using Verse;

    public static class BuildingPatches
    {
        [HarmonyPatch(
            typeof(Building),
            nameof(Building.DrawColor),
            MethodType.Getter)]
        public static class Building_DrawColorPatch
        {
            [HarmonyPrefix]
            static bool Prefix(Building __instance, ref Color __result)
            {
                if (__instance.Stuff != MixedStoneBlockDefs.MixedStoneBlocks)
                    return true;

                if (__instance.PaintColorDef != null)
                {
                    __result = __instance.PaintColorDef.color;
                }
                else
                {
                    StoneDefs.LoadDefs();

                    StoneDefs.GetColors(__instance.thingIDNumber, out var color, out var colorTwo);

                    __result = color;

                }

                return false;
            }
        }
    }
}