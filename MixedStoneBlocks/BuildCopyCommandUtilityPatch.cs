﻿namespace MixedStoneBlocks
{
    using HarmonyLib;
    using RimWorld;
    using UnityEngine;
    using Verse;

    [StaticConstructorOnStartup]
    public static class BuildCopyCommandUtilityPatch
    {
        private static readonly Texture2D MixedWallIcon;
        private static readonly Texture2D MixedDoorIcon;
        private static readonly Texture2D MixedAutodoorIcon;

        static BuildCopyCommandUtilityPatch()
        {
            MixedWallIcon = PatchGraphics.MixedWallIcon;
            MixedDoorIcon = PatchGraphics.MixedDoorIcon;
            MixedAutodoorIcon =PatchGraphics.MixedAutodoorIcon;
        }
        
        [HarmonyPatch(
            typeof(BuildCopyCommandUtility),
            nameof(BuildCopyCommandUtility.BuildCommand))]
        public static class BuildCommandPatch
        {
            [HarmonyPostfix]
            static void Postfix(BuildableDef buildable, ThingDef stuff, ref Command __result)
            {
                if (stuff != MixedStoneBlockDefs.MixedStoneBlocks) 
                    return;
                
                // Set "Build Copy" icon to static graphic
                if (buildable == ThingDefOf.Wall) 
                    __result.icon = MixedWallIcon;
                
                if (buildable == ThingDefOf.Door) 
                    __result.icon = MixedDoorIcon;

                if (buildable == MixedStoneBlockDefs.Autodoor)
                {
                    __result.icon = MixedAutodoorIcon;
                }
            }
        }
    }
}