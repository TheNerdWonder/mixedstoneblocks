﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MixedStoneBlocks
{
    using HarmonyLib;
    using RimWorld;
    using Verse;

    [DefOf]
    public static class MixedStoneBlockDefs
    {        
        // Materials
        public static ThingDef MixedStoneBlocks;

        // -----------------------------------
        // Graphic Atlas
        // -----------------------------------
        // Wall;
        public static ThingDef Fence;

        // -----------------------------------
        // Graphic_Single, static image
        // -----------------------------------
        // Column;
        // Door;
        public static ThingDef Autodoor;
        public static ThingDef FenceGate;


        static MixedStoneBlockDefs()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(Patch));
        }

        public static bool IsWall(BuildableDef def)
        {
            return def == ThingDefOf.Wall;
        }

        public static bool IsFence(BuildableDef def)
        {
            return def == Fence;
        }
    }

    [StaticConstructorOnStartup]
    public static class PatchGraphics
    {
        public static Texture2D MixedWallIcon { get; set; }
        public static Texture2D MixedDoorIcon { get; set; }
        public static Texture2D MixedAutodoorIcon { get; set; }

        public static Texture2D SculptureSmallMask { get; set; }
        public static Texture2D SculptureLargeMask { get; set; }
        public static Texture2D SculptureGrandMask { get; set; }
        
        static PatchGraphics()
        {
            MixedWallIcon = ContentFinder<Texture2D>.Get("Things_Buildings/MixedBlockWall_MenuIcon");
            MixedDoorIcon = ContentFinder<Texture2D>.Get("Things_Buildings/Door_MenuIcon");
            MixedAutodoorIcon = ContentFinder<Texture2D>.Get("Things_Buildings/Autodoor_MenuIcon");
            
            SculptureSmallMask = ContentFinder<Texture2D>.Get("Things_Buildings/SculptureSmall_defaultmask");
            SculptureLargeMask = ContentFinder<Texture2D>.Get("Things_Buildings/SculptureLarge_defaultmask");
            SculptureGrandMask = ContentFinder<Texture2D>.Get("Things_Buildings/SculptureGrand_defaultmask");
        }
    }
    
    public static class StoneDefs
    {
        public static List<ThingDef> BlockDefs;
        
        public static void LoadDefs()
        {
            if (BlockDefs == null)
            {
                BlockDefs =
                    DefDatabase<ThingDef>.AllDefsListForReading
                        .Where(def => def.defName.StartsWith("Block"))
                        .Where(def => def.stuffProps?.categories != null && def.stuffProps.categories.Contains(StuffCategoryDefOf.Stony))
                        .ToList();
                    
                Debug.Log($"Found {BlockDefs.Count} stone defs: {string.Join(", ", BlockDefs.Select(def => def.defName))}");
            }
        }

        public static void GetColors(int id, out Color color, out Color colorTwo, bool allowSimilarColors = true)
        {
            var index1 = id % BlockDefs.Count;
            var index2 = id * 37 % BlockDefs.Count;

            if (index1 == index2)
                index2 = (index2 + 17) % BlockDefs.Count;

            color = BlockDefs[index1].graphicData.color;
            colorTwo = BlockDefs[index2].graphicData.color;

            if (!allowSimilarColors &&
                ColorDistance(color, colorTwo) < 0.3)
            {
                if (index1 > index2)
                    color = BlockDefs[index1-1].graphicData.color;
                else
                    colorTwo = BlockDefs[index2-1].graphicData.color;
            }
        }

        // Computes "distance" between the RGB values of two colors
        private static double ColorDistance(Color color, Color colorTwo)
        {
            var rDist = color.r - colorTwo.r;
            var gDist = color.g - colorTwo.g;
            var bDist = color.b - colorTwo.b;

            return Math.Sqrt(rDist * rDist + gDist * gDist + bDist + bDist);
        }
    }
}