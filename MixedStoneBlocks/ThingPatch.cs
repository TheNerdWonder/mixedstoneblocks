﻿using System;
using System.Collections.Generic;

namespace MixedStoneBlocks
{
    using HarmonyLib;
    using RimWorld;
    using UnityEngine;
    using Verse;

    [StaticConstructorOnStartup]

    public static class ThingPatch
    {
        private static Graphic _mixedWallGraphic;
        private static Graphic _mixedFenceGraphic;
        private static Graphic _mixedDoorGraphic;
        private static Graphic _mixedAutodoorGraphic;
        private static Graphic _mixedGateGraphic;

        static ThingPatch()
        {
            // Putting this here to make the load warning shut up.
            //
            // The textures we need to create use the original graphic's size, so we can't load them here 
        }

        [HarmonyPatch(
            typeof(Thing),
            nameof(Thing.DrawColor),
            MethodType.Getter)]
        public static class Thing_DrawColorPatch
        {
            [HarmonyPrefix]
            static bool Prefix(Thing __instance, ref Color __result)
            {
                if (__instance.Stuff != MixedStoneBlockDefs.MixedStoneBlocks)
                    return true;

                StoneDefs.LoadDefs();

                StoneDefs.GetColors(__instance.thingIDNumber, out var color, out var colorTwo);

                __result = color;

                return false;
            }
        }

        [HarmonyPatch(
            typeof(Thing),
            nameof(Thing.DrawColorTwo),
            MethodType.Getter)]
        public static class Thing_DrawColorTwoPatch
        {
            [HarmonyPrefix]
            static bool Prefix(Thing __instance, ref Color __result)
            {
                var building = __instance as Building;

                if (__instance.Stuff != MixedStoneBlockDefs.MixedStoneBlocks)
                {
                    if (building != null)
                    {
                        // Buildings can just default to DrawColor to handle new complex masks
                        __result = building.DrawColor;
                    }
                    else
                    {
                        // Some non-building things have problems accessing DrawColor. Do it this way to match original method
                        __result = __instance.def.graphicData?.colorTwo ?? __instance.def.graphicData?.color ?? Color.white;
                    }
                }
                else if (building?.PaintColorDef != null)
                {
                    __result = building.PaintColorDef.color;
                }
                else
                {
                    StoneDefs.LoadDefs();

                    StoneDefs.GetColors(__instance.thingIDNumber, out var color, out var colorTwo);

                    __result = colorTwo;

                }

                return false;
            }
        }

        [HarmonyPatch(
            typeof(Thing),
            nameof(Thing.Print))]
        public static class PrintPatch
        {
            [HarmonyPrefix]
            static bool Prefix(SectionLayer layer, Thing __instance)
            {
                var building = __instance as Building;

                // Things with custom static graphics
                if (MixedStoneBlockDefs.IsWall(__instance.def))
                {
                    if (__instance.Stuff != MixedStoneBlockDefs.MixedStoneBlocks ||
                        building?.PaintColorDef != null)
                    {
                        return true;
                    }

                    LoadMixedStoneWallGraphics(__instance.Graphic);
                   _mixedWallGraphic.Print(layer, __instance, 0.0f);
                    return false;
                }

                if (MixedStoneBlockDefs.IsFence(__instance.def))
                {
                    if (__instance.Stuff != MixedStoneBlockDefs.MixedStoneBlocks ||
                        building?.PaintColorDef != null)
                    {
                        return true;
                    }

                    LoadMixedStoneFenceGraphics(__instance.Graphic);
                    _mixedFenceGraphic.Print(layer, __instance, 0.0f);
                    return false;
                }

                return true;
            }
        }
  
        // Fetches graphic for door animation
        [HarmonyPatch(
            typeof(Thing),
            nameof(Thing.Graphic),
            MethodType.Getter)]
        
        public static class GraphicGetterPatch
        {
            [HarmonyPrefix]
            static bool Prefix(Thing __instance, ref Graphic __result, ref Graphic ___styleGraphicInt)
            {
                var building = __instance as Building;

                if (building == null ||
                    __instance.Stuff != MixedStoneBlockDefs.MixedStoneBlocks ||
                    building.PaintColorDef != null)
                    return true;
                
                // Doors with custom static graphics
                var doorInstance = __instance as Building_Door;
                if (doorInstance?.def == ThingDefOf.Door)
                {
                    LoadMixedStoneDoorGraphics(__instance.DefaultGraphic);
                
                    __result = _mixedDoorGraphic;
                    return false;
                }

                if (doorInstance?.def == MixedStoneBlockDefs.Autodoor)
                {
                    if (__instance.StyleDef?.Graphic == null)
                    {
                        LoadMixedStoneDoorGraphics(__instance.DefaultGraphic);

                        __result = _mixedAutodoorGraphic;
                        return false;
                    }
                }

                if (doorInstance?.def == MixedStoneBlockDefs.FenceGate)
                {
                    LoadMixedStoneDoorGraphics(__instance.DefaultGraphic);

                    __result = _mixedGateGraphic;
                    return false;
                }

                return true;
            }
        }
        
        private static void LoadMixedStoneWallGraphics(Graphic origGraphic)
        {
            if (_mixedWallGraphic == null)
            {
                _mixedWallGraphic = GraphicDatabase.Get<Graphic_Single>("Things_Buildings/MixedBlockWall_Atlas",
                    ShaderTypeDefOf.Cutout.Shader, origGraphic.drawSize, Color.white);
                _mixedWallGraphic = GraphicUtility.WrapLinked(_mixedWallGraphic, LinkDrawerType.CornerFiller);
            }
        }

        private static void LoadMixedStoneFenceGraphics(Graphic origGraphic)
        {
            if (_mixedFenceGraphic == null)
            {
                _mixedFenceGraphic = GraphicDatabase.Get<Graphic_Single>("Things/Building/Linked/Fence/MixedStoneFence_Atlas",
                    ShaderTypeDefOf.Cutout.Shader, origGraphic.drawSize, Color.white);
                _mixedFenceGraphic = GraphicUtility.WrapLinked(_mixedFenceGraphic, LinkDrawerType.Asymmetric);
            }
        }

        private static void LoadMixedStoneDoorGraphics(Graphic graphic)
        {
            if (_mixedDoorGraphic == null)
            {
                _mixedDoorGraphic = GraphicDatabase.Get<Graphic_Single>("Things_Buildings/Door_Mover",
                    ShaderTypeDefOf.Cutout.Shader, graphic.drawSize, Color.white);
                
                _mixedAutodoorGraphic = GraphicDatabase.Get<Graphic_Single>("Things_Buildings/Autodoor_Mover",
                    ShaderTypeDefOf.Cutout.Shader, graphic.drawSize, Color.white);

                _mixedGateGraphic = GraphicDatabase.Get<Graphic_Multi>("Things/Building/Door/MixedStoneGate_Mover",
                    ShaderTypeDefOf.Cutout.Shader, graphic.drawSize, Color.white);
            }
        }
    }
}