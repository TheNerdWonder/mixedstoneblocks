A Rimworld mod to add walls and floors that can be constructed using any stone blocks. The blocks must first be combined into mixed block stacks at the stonecutter.

Fine stone tile floors are only added if you have the Royalty DLC installed.